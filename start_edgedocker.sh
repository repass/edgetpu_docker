#!/bin/sh
#this works if the coral device is on your first USB bus, which it is for me.  If you have more than one add --device=/dev/bus/usb/002 or --privileged which is not recommended but useful for testing.
docker run -h edgetpu -d --device=/dev/bus/usb/001 -v /home/repass/build/pixel-pics:/opt/my_pics -v /home/repass/src/edgetpu_docker/my_notebooks:/opt/my_notebooks -v /home/repass/src/edgetpu_docker/all_models:/opt/edgetpu/all_models -p 8888:8888 repass/edgetpu
